﻿using BeerOverflow.Models.Abstracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models
{
    public class Beer : Entity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 3)]
        public string Name { get; set; }

        [StringLength(500, MinimumLength = 10)]
        public string Description { get; set; }

        [Required,Range(0.00, 20.00)]
        public double ABV { get; set; }
        public Brewery Brewery { get; set; }
        public int BreweryId { get; set; }
        public Style Style { get; set; }
        public int StyleId { get; set; }

        public ICollection<BeerRating> Ratings { get; set; } = new List<BeerRating>();
        public ICollection<BeerReview> Reviews { get; set; } = new List<BeerReview>();
       
        //public double Price { get; set; }
        //public string ImageURL { get; set; }
        //public bool IsBeerOfTheWeek { get; set; }
        //public bool IsProduced { get; set; }
        //public bool IsPublished { get; set; }
        //public ICollection<Wishlist> Wishlists { get; set; } = new List<Wishlist>();

    }
}
