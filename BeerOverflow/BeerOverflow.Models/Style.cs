﻿using BeerOverflow.Models.Abstracts;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models
{
    public class Style:Entity
    {
        [Key]
        public int Id { get; set; }

        [Required,StringLength(40, MinimumLength = 2)]
        public string Name { get; set; }

    }
}
