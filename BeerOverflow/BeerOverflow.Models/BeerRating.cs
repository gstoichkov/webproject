﻿using BeerOverflow.Models.Abstracts;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models
{
    public class BeerRating:BeersUsers
    {
        [Required,Range(1,5)]
        public int Rating { get; set; }
        
    }
}
