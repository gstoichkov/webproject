﻿using BeerOverflow.Models.Abstracts;
using System;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models
{
    public class BeerReview:BeersUsers
    {
        [Key]
        public int Id { get; set; }

        [Required,StringLength(250, MinimumLength = 5)]
        public string Text { get; set; }


        //public DateTime CreatedOn { get; set; }
        //public int Flagged { get; set; }
        //public bool IsBanned { get; set; }
        //public int Rating { get; set; }
        //public bool IsDeleted { get; set; }
        //public Beer Beer { get; set; }
        //public int BeerId { get; set; }
    }
}
