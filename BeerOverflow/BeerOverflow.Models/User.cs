﻿using BeerOverflow.Models.Abstracts;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models
{
    public class User: IdentityUser<int>
    {
        //[Key]
        //public int Id { get; set; }
        //[Required,StringLength(50,MinimumLength =3)]
        //public string Name { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
        //Favorite beers
        public ICollection<WishListItem> Wishlist { get; set; } = new List<WishListItem>();
        //Reviewed beers
        public ICollection<BeerReview> Reviews { get; set; } = new List<BeerReview>();
        //Rated beers
        public ICollection<BeerRating> Ratings { get; set; } = new List<BeerRating>();



    }
}
