﻿using BeerOverflow.Models.Abstracts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models
{
    public class Brewery:Entity
    {
        [Key] 
        public int Id { get; set; }

        [Required,StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        public Country Country { get; set; }
        public int CountryId { get; set; }

        public ICollection<Beer> Beers { get; set; } = new List<Beer>();

    }
}
