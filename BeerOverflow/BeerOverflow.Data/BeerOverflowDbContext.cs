﻿using BeerOverflow.Data.DataConfigurations;
using BeerOverflow.Models;
using BeerOverflow.Models.Abstracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace BeerOverflow.Data
{
    public class BeerOverflowDbContext : IdentityDbContext<User, Role, int>
    {
        public BeerOverflowDbContext(DbContextOptions<BeerOverflowDbContext> options) : base(options)
        {

        }
        public DbSet<Beer> Beers { get; set; }
        public DbSet<Brewery> Breweries { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Style> Styles { get; set; }

        //public DbSet<User> Users{ get; set; }
        public DbSet<BeerRating> Ratings { get; set; }
        public DbSet<BeerReview> Reviews { get; set; }
        public DbSet<WishListItem> Wishlists { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BeerRatingConfig());
            modelBuilder.ApplyConfiguration(new BeerReviewConfig());
            modelBuilder.ApplyConfiguration(new WishListItemConfig());
            // for tests 
           // modelBuilder.Seed();
            base.OnModelCreating(modelBuilder);
        }
        

        public override int SaveChanges()
        {
            this.ChangeTracker.DetectChanges();
            var added = this.ChangeTracker.Entries()
                        .Where(t => t.State == EntityState.Added)
                        .Select(t => t.Entity)
                        .ToArray();
            foreach (var entity in added)
            {
                if (entity is Entity)
                {
                    var track = entity as Entity;
                    track.CreatedOn = DateTime.UtcNow;
                }
            }
            var modified = this.ChangeTracker.Entries()
                        .Where(t => t.State == EntityState.Modified)
                        .Select(t => t.Entity)
                        .ToArray();
            foreach (var entity in modified)
            {
                if (entity is Entity)
                {
                    var track = entity as Entity;
                    track.ModifiedOn = DateTime.UtcNow;
                }
            }
            return base.SaveChanges();
        }
    }
}
