﻿using BeerOverflow.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace BeerOverflow.Data
{
    public static class DataSeeder
    {

        public static void Seed(this ModelBuilder builder)
        {
            //Seed Roles
            // Normalized nameне позволява да се регистрират имене, които се различават само по casing
            var roles = new List<Role>()
            {
               new Role{ Id=1,Name="Admin",NormalizedName="ADMIN"}, 
               new Role{Id=2,Name="User",NormalizedName="USER"},
            };
            builder.Entity<Role>().HasData(roles);

            //Seed Admin Account
            var hasher = new PasswordHasher<User>(); // Returns a hash version of the typed password
            var adminUser = new User();
            adminUser.Id = 1;
            adminUser.UserName = "admin@admin.com";
            adminUser.NormalizedUserName = "ADMIN@ADMIN.COM";
            adminUser.Email = "admin@admin.com";
            adminUser.NormalizedEmail = "ADMIN@ADMIN.COM";
            adminUser.PasswordHash = hasher.HashPassword(adminUser, "admin123");
            adminUser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(adminUser);               //Adding the user
            builder.Entity<IdentityUserRole<int>>().HasData(         //Many yo many relationship with role
                new IdentityUserRole<int>()
                {
                    RoleId = 1,
                    UserId = adminUser.Id
                }
            );

            //Seeding user accounts
            //Alice
            var regularUser = new User();
            regularUser.Id = 2;
            regularUser.UserName = "alice@alice.com";
            regularUser.NormalizedUserName = "ALICE@ALICE.COM";
            regularUser.Email = "alice@alice.com";
            regularUser.NormalizedEmail = "ALICE@ALICE.COM";
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "alice123");
            regularUser.SecurityStamp = Guid.NewGuid().ToString(); 
            // ^^A random value that must change whenever a users credentials change (password changed, login removed)
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>()
                {
                    RoleId = 2,
                    UserId = regularUser.Id
                }
            );
            // Bob
            regularUser = new User();
            regularUser.Id = 3;
            regularUser.UserName = "bob@bob.com";
            regularUser.NormalizedUserName = "BOB@BOB.COM";
            regularUser.Email = "bob@bob.com";
            regularUser.NormalizedEmail = "BOB@BOB.COM";
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "bob123");
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>()
                {
                    RoleId = 2,
                    UserId = regularUser.Id
                }
            );

            //Seed Countries
            var countries = new List<Country>()
            {
               new Country{ Id=1,Name= "Germany"},
               new Country{ Id=2,Name= "USA"},
               new Country{ Id=3, Name= "Brazil"},
               new Country{ Id=4,Name= "Czech Republic"},
               new Country{ Id=5, Name= "Mexico"},
               new Country{ Id=6, Name= "Russia"},
               new Country{ Id=7,Name= "Japan"},
               new Country{ Id=8, Name= "Uk"},
               new Country{ Id=9, Name= "Poland"},
               new Country{ Id=10, Name= "Belgium"},
            };
            builder.Entity<Country>().HasData(countries);

            //Seed Beers
            var beers = new List<Beer>()
            {
              new Beer
              {
                Id=1,
                Name= "Maibock",
                StyleId=1,
                ABV=0.00,
                BreweryId=1,
                Description=
              "The German-style Maibock is paler in color " +
              "and more hop-centric than traditional bock beers. A lightly toasted and/or bready malt " +
              "character is often evident." 
              },
              new Beer{Id=2, Name= "Free Bike Amber",StyleId=2,ABV=4.50,BreweryId=2,Description=" We're Sorry :( There is no description available for this item yey!"},
              new Beer{Id=3, Name= "Smooth Hoperator",StyleId=3,ABV=7.00,BreweryId=1,Description="This medium bodied, copper colored strong lager is in a class all by itself.Available twice a year in October and April."},
              new Beer{Id=4, Name= "Round Barn Cocoa Stout",StyleId=4,ABV=0.00,BreweryId=4,},
              new Beer{Id=5, Name= "Pillar Point Pale Ale",StyleId=5,ABV=4.6,BreweryId=1,},
              new Beer{Id=6, Name= "Alembic Pale",StyleId=5,ABV=5.5,BreweryId=1,Description="Bright clear copper color. Almost no aroma, just a faint bit of caramel. Starts sweet and caramely, turning just slightly bitter midway through."},
              new Beer{Id=7, Name= "Resurrection",StyleId=6,ABV=7.00,BreweryId=1,Description="During the fermentation of the first batch the yeast died and was resurrected by brewer Chris Cashell.This beer is quite strong and flavorful, without being too sweet."},
              new Beer
              {
                Id=8,
                Name= "Kuronama",
                StyleId=7,
                ABV=0.00,
                BreweryId=8,
                Description=
              "Production in Japan was stopped in September 2015." +
              "Bottle & can: Pasteurised. Ingredients: Dark roasted malt; hops; rice; maize; " 
              },
              new Beer{Id=9, Name= "Baltika 6 Porter",StyleId=7,ABV=7.00,BreweryId=9,Description="Look. Dark brown to black, medium khaki head.Smell. Dark roasted malt, dark dried fruit, raisin, prune, etc."},
              new Beer{Id=10, Name= "Porter Boom",StyleId=7,ABV=8.00,BreweryId=10,Description="In general, the porter is a top-fermented beer that uses black or chocolate malts to create a beer that ranges in color from dark brown to almost black."}
            };
            builder.Entity<Beer>().HasData(beers);

            //Seed Styles
            var styles = new List<Style>()
            {
              new Style{ Id=1, Name="Traditional German-Style Bock"},
              new Style{ Id=2, Name="American-Style Amber"},
              new Style{ Id=3, Name="German-Style Doppelbock"},
              new Style{ Id=4, Name="Sweet Stout"},
              new Style{ Id=5, Name="American-Style Pale Ale"},
              new Style{ Id=6, Name="Belgian-Style Dubbel"},
              new Style{ Id=7, Name="Porter"},
            };
            builder.Entity<Style>().HasData(styles);

            //Seed Breweries
            var breweries = new List<Brewery>()
            {
                new Brewery{ Id=1, Name="Scuttlebutt Brewing",CountryId=1},
                new Brewery{ Id=2, Name="Skagit River Brewing",CountryId=2},
                new Brewery{ Id=3, Name="Stoudt's Brewery",CountryId=1},
                new Brewery{ Id=4, Name="The Round Barn Winery & Brew",CountryId=8},
                new Brewery{ Id=5, Name="Half Moon Bay Brewing",CountryId=2},
                new Brewery{ Id=6, Name="Elliott Bay Brewery and Pub",CountryId=2},
                new Brewery{ Id=7, Name="Brewer's Art",CountryId=10},
                new Brewery{ Id=8, Name="Asahi Breweries",CountryId=7},
                new Brewery{ Id=9, Name="Pivzavod Baltika",CountryId=6},
                new Brewery{ Id=10, Name="Pivovar Pardubice a.s",CountryId=4},
            };
            builder.Entity<Brewery>().HasData(breweries);
        }

    }
}
