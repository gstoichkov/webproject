﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BeerOverflow.Data.DataConfigurations
{
    public class BeerReviewConfig : IEntityTypeConfiguration<BeerReview>
    {
        public void Configure(EntityTypeBuilder<BeerReview> builder)
        {
            builder.HasKey(e => new { e.BeerId, e.UserId });
        }
    }
}
