﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BeerOverflow.Data.DataConfigurations
{
    public class BeerRatingConfig:IEntityTypeConfiguration<BeerRating>
    {
        public void Configure(EntityTypeBuilder<BeerRating> builder)
        {
            builder.HasKey(e => new { e.BeerId, e.UserId });
        }
    }
}
