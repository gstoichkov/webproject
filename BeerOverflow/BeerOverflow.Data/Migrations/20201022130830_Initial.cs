﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BeerOverflow.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Styles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 40, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Styles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Breweries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    CountryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Breweries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Breweries_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Beers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 40, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    ABV = table.Column<double>(nullable: false),
                    BreweryId = table.Column<int>(nullable: false),
                    StyleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Beers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Beers_Breweries_BreweryId",
                        column: x => x.BreweryId,
                        principalTable: "Breweries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Beers_Styles_StyleId",
                        column: x => x.StyleId,
                        principalTable: "Styles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ratings",
                columns: table => new
                {
                    BeerId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ratings", x => new { x.BeerId, x.UserId });
                    table.ForeignKey(
                        name: "FK_Ratings_Beers_BeerId",
                        column: x => x.BeerId,
                        principalTable: "Beers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Ratings_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reviews",
                columns: table => new
                {
                    BeerId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    Text = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reviews", x => new { x.BeerId, x.UserId });
                    table.ForeignKey(
                        name: "FK_Reviews_Beers_BeerId",
                        column: x => x.BeerId,
                        principalTable: "Beers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Reviews_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Wishlists",
                columns: table => new
                {
                    BeerId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wishlists", x => new { x.BeerId, x.UserId });
                    table.ForeignKey(
                        name: "FK_Wishlists_Beers_BeerId",
                        column: x => x.BeerId,
                        principalTable: "Beers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Wishlists_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "b045d7ca-06b9-46b4-9ed3-7300baf721bc", "Admin", "ADMIN" },
                    { 2, "38554418-05d5-4ac0-aad1-b9f52cb59e14", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CreatedOn", "DeletedOn", "Email", "EmailConfirmed", "IsDeleted", "LockoutEnabled", "LockoutEnd", "ModifiedOn", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 1, 0, "46601b77-d336-4192-bf99-3ebc1388057f", new DateTime(2020, 10, 22, 13, 8, 30, 110, DateTimeKind.Utc).AddTicks(3648), null, "admin@admin.com", false, false, false, null, null, "ADMIN@ADMIN.COM", "ADMIN@ADMIN.COM", "AQAAAAEAACcQAAAAELMmuwcv9UfQTuzx9OaGaDZbO5czivzYlh1aLjJSchTWCIBWvrmwY49b3wStCUcyyA==", null, false, "28d5079e-4335-4796-bbad-c73ef0279cbc", false, "admin@admin.com" },
                    { 2, 0, "00e58089-770c-433f-8f07-daeda3373b96", new DateTime(2020, 10, 22, 13, 8, 30, 123, DateTimeKind.Utc).AddTicks(1966), null, "alice@alice.com", false, false, false, null, null, "ALICE@ALICE.COM", "ALICE@ALICE.COM", "AQAAAAEAACcQAAAAEA9aKjIywT2hvhAnpY3NucKVsBfOJhkUTEPenpHrPWJ3wANc6bhDUw1aABSC3O5ABg==", null, false, "3fd9b184-2206-436f-a011-aa2a1b8ac4f0", false, "alice@alice.com" },
                    { 3, 0, "4c301859-8aa4-45ec-9435-e6075506f6ea", new DateTime(2020, 10, 22, 13, 8, 30, 129, DateTimeKind.Utc).AddTicks(7361), null, "bob@bob.com", false, false, false, null, null, "BOB@BOB.COM", "BOB@BOB.COM", "AQAAAAEAACcQAAAAEDjDIicAMm3f1IL6/U9ylHiPAUgBTq5Vqq89DYv+ntqco7BiFIKJL9wSZD9Mm71ZKA==", null, false, "439defe3-3009-477c-9e7c-0a2fba455910", false, "bob@bob.com" }
                });

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 6, new DateTime(2020, 10, 22, 13, 8, 30, 136, DateTimeKind.Utc).AddTicks(5555), null, false, null, "Russia" },
                    { 2, new DateTime(2020, 10, 22, 13, 8, 30, 136, DateTimeKind.Utc).AddTicks(5529), null, false, null, "USA" },
                    { 5, new DateTime(2020, 10, 22, 13, 8, 30, 136, DateTimeKind.Utc).AddTicks(5550), null, false, null, "Mexico" },
                    { 3, new DateTime(2020, 10, 22, 13, 8, 30, 136, DateTimeKind.Utc).AddTicks(5547), null, false, null, "Brazil" },
                    { 4, new DateTime(2020, 10, 22, 13, 8, 30, 136, DateTimeKind.Utc).AddTicks(5548), null, false, null, "Czech Republic" },
                    { 1, new DateTime(2020, 10, 22, 13, 8, 30, 136, DateTimeKind.Utc).AddTicks(4491), null, false, null, "Germany" },
                    { 9, new DateTime(2020, 10, 22, 13, 8, 30, 136, DateTimeKind.Utc).AddTicks(5559), null, false, null, "Poland" },
                    { 8, new DateTime(2020, 10, 22, 13, 8, 30, 136, DateTimeKind.Utc).AddTicks(5558), null, false, null, "Uk" },
                    { 7, new DateTime(2020, 10, 22, 13, 8, 30, 136, DateTimeKind.Utc).AddTicks(5556), null, false, null, "Japan" },
                    { 10, new DateTime(2020, 10, 22, 13, 8, 30, 136, DateTimeKind.Utc).AddTicks(5561), null, false, null, "Belgium" }
                });

            migrationBuilder.InsertData(
                table: "Styles",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(4593), null, false, null, "Traditional German-Style Bock" },
                    { 3, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(5798), null, false, null, "German-Style Doppelbock" },
                    { 4, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(5809), null, false, null, "Sweet Stout" },
                    { 5, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(5810), null, false, null, "American-Style Pale Ale" },
                    { 6, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(5813), null, false, null, "Belgian-Style Dubbel" },
                    { 7, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(5814), null, false, null, "Porter" },
                    { 2, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(5763), null, false, null, "American-Style Amber" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 },
                    { 3, 2 }
                });

            migrationBuilder.InsertData(
                table: "Breweries",
                columns: new[] { "Id", "CountryId", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2020, 10, 22, 13, 8, 30, 138, DateTimeKind.Utc).AddTicks(1424), null, false, null, "Scuttlebutt Brewing" },
                    { 3, 1, new DateTime(2020, 10, 22, 13, 8, 30, 138, DateTimeKind.Utc).AddTicks(3314), null, false, null, "Stoudt's Brewery" },
                    { 2, 2, new DateTime(2020, 10, 22, 13, 8, 30, 138, DateTimeKind.Utc).AddTicks(3210), null, false, null, "Skagit River Brewing" },
                    { 5, 2, new DateTime(2020, 10, 22, 13, 8, 30, 138, DateTimeKind.Utc).AddTicks(3326), null, false, null, "Half Moon Bay Brewing" },
                    { 6, 2, new DateTime(2020, 10, 22, 13, 8, 30, 138, DateTimeKind.Utc).AddTicks(3329), null, false, null, "Elliott Bay Brewery and Pub" },
                    { 10, 4, new DateTime(2020, 10, 22, 13, 8, 30, 138, DateTimeKind.Utc).AddTicks(3335), null, false, null, "Pivovar Pardubice a.s" },
                    { 9, 6, new DateTime(2020, 10, 22, 13, 8, 30, 138, DateTimeKind.Utc).AddTicks(3333), null, false, null, "Pivzavod Baltika" },
                    { 8, 7, new DateTime(2020, 10, 22, 13, 8, 30, 138, DateTimeKind.Utc).AddTicks(3332), null, false, null, "Asahi Breweries" },
                    { 4, 8, new DateTime(2020, 10, 22, 13, 8, 30, 138, DateTimeKind.Utc).AddTicks(3323), null, false, null, "The Round Barn Winery & Brew" },
                    { 7, 10, new DateTime(2020, 10, 22, 13, 8, 30, 138, DateTimeKind.Utc).AddTicks(3330), null, false, null, "Brewer's Art" }
                });

            migrationBuilder.InsertData(
                table: "Beers",
                columns: new[] { "Id", "ABV", "BreweryId", "CreatedOn", "DeletedOn", "Description", "IsDeleted", "ModifiedOn", "Name", "StyleId" },
                values: new object[,]
                {
                    { 1, 0.0, 1, new DateTime(2020, 10, 22, 13, 8, 30, 136, DateTimeKind.Utc).AddTicks(7887), null, "The German-style Maibock is paler in color and more hop-centric than traditional bock beers. A lightly toasted and/or bready malt character is often evident.", false, null, "Maibock", 1 },
                    { 3, 7.0, 1, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(1557), null, "This medium bodied, copper colored strong lager is in a class all by itself.Available twice a year in October and April.", false, null, "Smooth Hoperator", 3 },
                    { 5, 4.5999999999999996, 1, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(1563), null, null, false, null, "Pillar Point Pale Ale", 5 },
                    { 6, 5.5, 1, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(1580), null, "Bright clear copper color. Almost no aroma, just a faint bit of caramel. Starts sweet and caramely, turning just slightly bitter midway through.", false, null, "Alembic Pale", 5 },
                    { 7, 7.0, 1, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(1582), null, "During the fermentation of the first batch the yeast died and was resurrected by brewer Chris Cashell.This beer is quite strong and flavorful, without being too sweet.", false, null, "Resurrection", 6 },
                    { 2, 4.5, 2, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(1202), null, " We're Sorry :( There is no description available for this item yey!", false, null, "Free Bike Amber", 2 },
                    { 10, 8.0, 10, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(1591), null, "In general, the porter is a top-fermented beer that uses black or chocolate malts to create a beer that ranges in color from dark brown to almost black.", false, null, "Porter Boom", 7 },
                    { 9, 7.0, 9, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(1586), null, "Look. Dark brown to black, medium khaki head.Smell. Dark roasted malt, dark dried fruit, raisin, prune, etc.", false, null, "Baltika 6 Porter", 7 },
                    { 8, 0.0, 8, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(1584), null, "Production in Japan was stopped in September 2015.Bottle & can: Pasteurised. Ingredients: Dark roasted malt; hops; rice; maize; ", false, null, "Kuronama", 7 },
                    { 4, 0.0, 4, new DateTime(2020, 10, 22, 13, 8, 30, 137, DateTimeKind.Utc).AddTicks(1562), null, null, false, null, "Round Barn Cocoa Stout", 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_BreweryId",
                table: "Beers",
                column: "BreweryId");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_StyleId",
                table: "Beers",
                column: "StyleId");

            migrationBuilder.CreateIndex(
                name: "IX_Breweries_CountryId",
                table: "Breweries",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Ratings_UserId",
                table: "Ratings",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_UserId",
                table: "Reviews",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Wishlists_UserId",
                table: "Wishlists",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Ratings");

            migrationBuilder.DropTable(
                name: "Reviews");

            migrationBuilder.DropTable(
                name: "Wishlists");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Beers");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Breweries");

            migrationBuilder.DropTable(
                name: "Styles");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
