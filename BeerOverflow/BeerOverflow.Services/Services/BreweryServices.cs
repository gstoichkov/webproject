﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.DTOMappers;
using Elasticsearch.Net;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Services
{
    public class BreweryServices : IBreweryService
    {
        private readonly BeerOverflowDbContext db;
       // private readonly IDateTimeProvider dateTimeProvider;

        public BreweryServices(BeerOverflowDbContext db)
        {
            this.db = db;
          //  this.dateTimeProvider = dateTimeProvider;
        }
        public async Task<ICollection<BreweryDTO>> GetAllBreweries(int itemsPerPage = 5, int pageIndex = 1)
        {
            var breweryList =
              await this.db.Breweries
              .Include(br=>br.Country)
               .Skip((pageIndex - 1) * itemsPerPage)
               .Take(itemsPerPage)
               .ToListAsync();
            return breweryList.GetDTO();
        }
        public async Task<ICollection<BreweryDTO>> GetAllForDisplay()
        {
            var breweryList =
              await this.db.Breweries
              .Include(br => br.Country)
               .ToListAsync();
            return breweryList.GetDTO();
        }
        public async Task<ICollection<BeerDTO>> GetBeers(int id)
        {
            var brewery = await db.Breweries
                .Include(br=>br.Beers)
                .FirstOrDefaultAsync(brewery => brewery.Id ==id);
            if(brewery==null)
            {
                throw new ArgumentNullException();
            }
            var collection =  brewery.Beers;
            //var nameCollection = new List<string>();
            //foreach (var item in collection)
            //{
            //    nameCollection.Add(item.Name);
            //}
           
            return collection.GetDTO();
           
        }
        public async Task<int> GetBreweriesCount() => await db.Breweries.CountAsync();
        public async Task<BreweryDTO> GetBrewery(int id)
        {
            var brewery = await db.Breweries
                .Include(br=>br.Country)
                .Include(br=>br.Beers)
                .FirstOrDefaultAsync(brewery => brewery.Id == id);
            if (brewery == null)
            {
                throw new ArgumentNullException("Brewery doesn't exist");
            }
            return brewery.GetDTO();
        }

        public async Task CreateBrewery(BreweryDTO model)
        {
            if (await db.Breweries.AnyAsync(b => b.Name == model.Name))
            {
                throw new ArgumentException("Brewery with such name already exists!");
            }
            var newBrewery = new Brewery
            {
                Name = model.Name,
                CountryId = model.CountryId,
            };
           await db.Breweries.AddAsync(newBrewery);
          await  db.SaveChangesAsync();
        }

        public async Task EditBrewery(int id, BreweryDTO model)
        {
            var oldBrewery =
               await db.Breweries
                .Include(b => b.Country)
                 .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            if (oldBrewery == null)
            {
                throw new ArgumentNullException();
            }

            oldBrewery.Name = model.Name;
            oldBrewery.CountryId = model.CountryId;
            oldBrewery.ModifiedOn = DateTime.UtcNow;
           await db.SaveChangesAsync();
        }

        public async Task DeleteBrewery(int id)
        {
            var brewery = await db.Breweries
                .FirstOrDefaultAsync(brewery => brewery.Id == id && brewery.IsDeleted == false);
            if (brewery == null)
            {
                throw new ArgumentNullException("Brewery doesn't exist");
            }
            brewery.IsDeleted = true;
            brewery.DeletedOn = DateTime.UtcNow;
            await db.SaveChangesAsync();
        }
    }
}

