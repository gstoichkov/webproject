﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DTOMappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Services
{
    public class StyleServices : IStyleService
    {
        private readonly BeerOverflowDbContext db;
        public StyleServices(BeerOverflowDbContext db)
        {
            this.db = db;
        }
        public async Task<ICollection<StyleDTO>> GetAllStyles(int itemsPerPage = 5, int pageIndex = 1)
        {
            var styleList =
                await this.db.Styles
                 .Where(x => x.IsDeleted == false)
                 .Skip((pageIndex - 1) * itemsPerPage)
                 .Take(itemsPerPage)
                 .ToListAsync();
               
            return styleList.GetDTO();
        }
        public async Task<ICollection<StyleDTO>> GetAllForDisplay()
        {
            var styleList =
                await this.db.Styles
                 .Where(x => x.IsDeleted == false)
                 .ToListAsync();

            return styleList.GetDTO();
        }
        public async Task<StyleDTO> GetSyle(int id)
        {
            var style = await db.Styles
                .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
                
            if (style == null)
            {
                throw new ArgumentNullException();
            }
            return style.GetDTO();
        }

        public async Task CreateStyle(StyleDTO style)
        {
           
          if (await db.Styles.AnyAsync(b => b.Name == style.Name))
            {
                throw new ArgumentException("Style with such name already exists!");
            }
            var newStyle = new Style
            {
                Name = style.Name,
         
            };
            await db.Styles.AddAsync(newStyle);
            await db.SaveChangesAsync();
        }

        public async Task DeleteStyle(int id)
        {
            var style = await db.Styles
                .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted==false);

            if (style == null)
            {
                throw new ArgumentNullException("Style doesn't exist");
            }

            style.IsDeleted = true;
            style.DeletedOn = DateTime.UtcNow;
           await db.SaveChangesAsync();
        }

        public async Task EditStyle(int id, StyleDTO model)
        {
            var oldStyle = await db.Styles
                .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            if (oldStyle == null)
            {
                throw new ArgumentNullException("No such style exists in our database!");
            }
            oldStyle.Name = model.Name;
            await db.SaveChangesAsync();
        }
    }
}
