﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.DTOMappers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Services
{
    public class UserServices : IUserService
    {
        private readonly BeerOverflowDbContext db;
        private readonly UserManager<User> userManager;

        public UserServices(BeerOverflowDbContext db, UserManager<User> userManager)
        {
            this.db = db;
            this.userManager = userManager;
        }
        public async Task<ICollection<UserDTO>> GetAllUsers(int itemsPerPage = 5, int pageIndex = 1)
        {

            var userList = await db.Users
                .Where(x => x.IsDeleted == false)
                .Skip((pageIndex - 1) * itemsPerPage)
                .Take(itemsPerPage)
                 .ToListAsync();
               
            return userList.GetDTO();
        }
        public async Task<IList<User>> GetAllRegularUsers()
        {
            var regularUsers = new List<User>();
            var users = this.db.Users.ToList();
            foreach (var u in users)
            {
                if (await this.userManager.IsInRoleAsync(u, "User"))
                {
                    regularUsers.Add(u);
                }
            }
            //var regularUsers = this.dbContext.Users.Where(u => this.userManager.IsInRoleAsync(u, "User").Result).ToList();
            return regularUsers;

        }
        public async Task<UserDTO> GetUser(int id)
        {
            var user = await db.Users
               .Where(x => x.IsDeleted == false)
               .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new ArgumentNullException("User doesn't exist");
            }
            return user.GetDTO();
        }
        public async Task CreateUser(UserDTO user)
        {
            var userEntity = new User
            {
                UserName = user.Name,
                
            };
           await db.Users.AddAsync(userEntity);
           await db.SaveChangesAsync();
        }

        public async Task UpdateUser(int id, UserDTO model)
        {
            var user = await db.Users
                .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
            if (user == null)
            {
                throw new ArgumentNullException("User doesn't exist");
            }
            user.UserName = model.Name;
            
           await db.SaveChangesAsync();
        }

        public async Task DeleteUser(int id)
        {
            var user = await db.Users.FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
            if (user == null)
            {
                throw new ArgumentNullException("User doesn't exist!");
            }
            user.IsDeleted = true;
            user.DeletedOn = DateTime.UtcNow;
            await db.SaveChangesAsync();
        }

        public async Task AddBeerToWishlist(int userId, BeerDTO dto)
        {
            var user = await this.db.Users.FirstOrDefaultAsync(user => user.Id == userId && user.IsDeleted == false);
            var beer = await db.Beers.FirstOrDefaultAsync(beer => beer.Id == dto.Id && beer.IsDeleted == false);
            if (user == null)
            {
                throw new ArgumentNullException("User doesn't exist!");
            }
            else if (beer == null)
            {
                throw new ArgumentNullException("Beer doesn't exist!");
            }
            var item = new WishListItem
            {
                BeerId = beer.Id,
                UserId = user.Id
            };
         user.Wishlist.Add(item);
          await  db.SaveChangesAsync();
        }
        public async Task<ICollection<BeerDTO>> GetBeerWishList(int id)
        {
            var wishlist = await db.Wishlists
                .Where(user => user.UserId == id && user.IsDeleted == false)
                .Select(beer => beer.Beer).ToListAsync();

            return wishlist.GetDTO();
        }
    }
}
