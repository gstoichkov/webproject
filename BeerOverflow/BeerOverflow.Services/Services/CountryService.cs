﻿using BeerOverflow.Data;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.DTOMappers;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using BeerOverflow.Models;

namespace BeerOverflow.Services
{
    public class CountryService : ICountryService
    {
        private readonly BeerOverflowDbContext db;
        public CountryService(BeerOverflowDbContext db)
        {
            this.db = db;
        }

        public async Task<IEnumerable<CountryDTO>> GetAllCountries(int itemsPerPage = 5, int pageIndex = 1)
        {
            var countryList =
           await this.db.Countries
                // .Where(x => x.IsDeleted == false)
                .Skip((pageIndex - 1) * itemsPerPage)
                .Take(itemsPerPage)
                .ToListAsync();

            return countryList.GetDTO();
        }

        public async Task<int> GetCountriesCount() => await db.Countries.CountAsync();

        public async Task CreateCountry(CountryDTO countryDTO)
        {
            if (countryDTO == null)
            {
                throw new ArgumentNullException();
            }
            else if (await db.Countries.AnyAsync(c => c.Name == countryDTO.Name))
            {
                throw new ArgumentException("Country with such name already exists!");
            }

            var country = new Country
            {
                Name = countryDTO.Name,
            };

            await db.Countries.AddAsync(country);

            await db.SaveChangesAsync();
        }

        public async Task<CountryDTO> GetCountry(int id)
        {
            var country =
                await db.Countries
                .FirstOrDefaultAsync(x => x.Id == id);
            if (country == null)
            {
                throw new ArgumentNullException("Country doesn't exist");
            }
            return country.GetDTO();
        }
        public async Task UpdateCountry(int id, CountryDTO model)
        {
            var oldCountry = await db.Countries
               .FirstOrDefaultAsync(beer => beer.Id == id && beer.IsDeleted == false);

            if (oldCountry == null)
            {
                throw new ArgumentNullException();
            }
            oldCountry.Name = model.Name;
            oldCountry.ModifiedOn = DateTime.UtcNow;
            await db.SaveChangesAsync();
        }

        public async Task DeleteCountry(int id)
        {
            var country =
               await db.Countries.FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
            if (country == null)
            {
                throw new ArgumentNullException("Country doesn't exist!");
            }
            country.IsDeleted = true;
            country.DeletedOn = DateTime.UtcNow;
            await db.SaveChangesAsync();
        }
    }
}
