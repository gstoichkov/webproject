﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DTOMappers;
using Elasticsearch.Net;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public class BeerServices : IBeerServices
    {
        private readonly BeerOverflowDbContext db;

        public BeerServices(BeerOverflowDbContext db)
        {
            this.db = db;
           
        }
        public async Task<ICollection<BeerDTO>> TopRatedBeers()
        {
            var collection =
                await db.Beers
                .OrderByDescending(beer => beer.Ratings.Select(rating=>rating.Rating).Average())
                .Take(6)
                .ToListAsync();
            return collection.GetDTO();
                
        }
  
        //Get ALL Beers 
        public async Task<ICollection<BeerDTO>> GetAllBeers(int itemsPerPage = 5, int pageIndex = 1)
        {
            var beerList = await this.db.Beers
                //.Where(x => x.IsDeleted == false)
                .Skip((pageIndex - 1) * itemsPerPage)
                .Take(itemsPerPage)
                .Include(b => b.Style)
                .Include(b => b.Brewery)
                .ToListAsync();

            await db.SaveChangesAsync();

            return beerList.GetDTO();
        }
        public async Task<int> GetBeersCount() => await db.Beers.CountAsync();

        //GET Beer by ID
        public async Task<BeerDTO> GetBeer(int id)
        {
            var beer = await db.Beers
                //.Include(b => b.Style)
                .Include(b => b.Brewery)
                .FirstOrDefaultAsync(beer => beer.Id == id);//&& beer.IsDeleted == false);

            if (beer == null)
            {
                throw new ArgumentNullException();
            }
            await db.SaveChangesAsync();
            return beer.GetDTO();
        }

        //CREATE Beer
        public async Task  CreateBeer(BeerDTO beer)
        {
            if (beer == null)
            {
                throw new ArgumentNullException();
            }
            else if (await db.Beers.AnyAsync(b => b.Name == beer.Name))
            {
                throw new ArgumentException("Beer with such name already exists!");
            }
            var newBeer = new Beer
            {
                Name = beer.Name,
                Description = beer.Description,
                ABV = beer.ABV,
                BreweryId = beer.BreweryId,
                StyleId = beer.StyleId,
            };
            await db.Beers.AddAsync(newBeer);
            await db.SaveChangesAsync();
        }

        //SEARCH for Beers
        public async Task<ICollection<BeerDTO>> GetBeerByCriteria(string name, double? abv, string breweryName)
        {
            var collection = await db.Beers
                .Where(x => !x.IsDeleted
                && name != null ? x.Name == name : true
                && abv.HasValue ? x.ABV == abv.Value : true
                && breweryName != null ? x.Brewery.Name == breweryName : true)
               .Include(b => b.Style)
               .Include(b => b.Brewery)
               .ToListAsync();
            return collection.GetDTO();
        }

        //GET Beer Reviews
        public async Task<ICollection<BeerReviewDTO>> GetBeerReviews(int id)
        {
            var beer = await db.Beers
                .Include(b => b.Reviews)
                .FirstOrDefaultAsync(beer => beer.Id == id && beer.IsDeleted == false);

            if (beer == null)
            {
                throw new ArgumentNullException("Beer has no reviews!");
            }

            var beerReviews = beer.Reviews.GetDTO();
            return beerReviews;
        }

        //RATE Beer
        public async Task RateBeer(int id, int rating, int userId)
        {
            var beer = await db.Beers
                //.Include(b=>b.Ratings)
                .FirstOrDefaultAsync(beer => beer.Id == id && beer.IsDeleted == false);

            if (beer == null)
            {
                throw new ArgumentNullException();
            }

            var newRating = new BeerRating
            {
                BeerId = beer.Id,
                UserId = userId,
                Rating = rating
            };

            beer.Ratings.Add(newRating);
            await db.SaveChangesAsync();
        }

        //REVIEW Beer
        public async Task ReviewBeer(int id, string text, int userId)
        {
            var beer = await db.Beers
               .Include(b => b.Style)
               .Include(b => b.Brewery)
               .FirstOrDefaultAsync(beer => beer.Id == id && beer.IsDeleted == false);

            if (beer == null)
            {
                throw new ArgumentNullException();
            }

            var newReview = new BeerReview
            {
                BeerId = beer.Id,
                UserId = userId,
                Text = text
            };

            beer.Reviews.Add(newReview);
            await db.SaveChangesAsync();
        }
        //EDIT Beer 
        public async Task EditBeer(int id, BeerDTO beer)
        {
            var oldBeer = await db.Beers
               .Include(b => b.Style)
               .Include(b => b.Brewery)
               .FirstOrDefaultAsync(beer => beer.Id == id && beer.IsDeleted == false);
            if (oldBeer == null)
            {
                throw new ArgumentNullException();
            }
            oldBeer.Name = beer.Name;
            oldBeer.Description = beer.Description;
            oldBeer.ABV = beer.ABV;
            oldBeer.BreweryId = beer.BreweryId;
            oldBeer.StyleId = beer.StyleId;
            await db.SaveChangesAsync();
        }

        //DELETE  Beer by ID 
        public async Task DeleteBeer(int id)
        {
            var beer = await db.Beers
                .Include(b => b.Style)
                .Include(b => b.Brewery)
                .FirstOrDefaultAsync(beer => beer.Id == id && beer.IsDeleted == false);

            if (beer == null)
            {
                throw new ArgumentNullException("Beer doesn't exist!");
            }
            beer.IsDeleted = true;
            beer.DeletedOn = DateTime.UtcNow;
            await db.SaveChangesAsync();
        }
        //FILTER Beers
        public async Task<ICollection<BeerDTO>> FilterBeers(string brewery, string style)
        {
            if (brewery != null && style == null)
            {
                if (await db.Beers.AnyAsync(Beer => Beer.Brewery.Name == brewery))
                {
                    var firstFilter =
                     await db.Beers
                        .Include(b => b.Brewery)
                        .Where(beer => beer.Brewery.Name == brewery && beer.IsDeleted == false)
                        .ToListAsync();
                    return firstFilter.GetDTO();
                }
            }
            if (brewery == null && style != null)
            {
                if (await db.Beers.AnyAsync(Beer => Beer.Style.Name == style))
                {
                    var secondFilter = await db.Beers
                        .Include(b => b.Style)
                        .Where(beer => beer.Style.Name == style && beer.IsDeleted == false)
                        .ToListAsync();
                    return secondFilter.GetDTO();
                }
            }
            var collection =
            await db.Beers
                .Include(b => b.Style)
                .Include(b => b.Brewery)
                .Where(beer => beer.Brewery.Name == brewery && beer.Style.Name == style && beer.IsDeleted == false)
                .ToListAsync();
            return collection.GetDTO();
        }

        //ORDER Beers
        public async Task<ICollection<BeerDTO>> SortBeers(string criteria)
        {
            if (criteria == "name")
            {
                var collection = await
               db.Beers.Where(beer => beer.IsDeleted == false)
                    .OrderBy(beer => beer.Name).
                    ToListAsync();
                return collection.GetDTO();
            }
            else if (criteria == "ABV")
            {
                var collection = await
                 db.Beers.Where(beer => beer.IsDeleted == false)
                    .OrderBy(beer => beer.ABV).
                    ToListAsync();
                return collection.GetDTO();
            }
            else if (criteria == "rating")
            {
                var collection = await
                db.Beers.Where(beer => beer.IsDeleted == false)
                        .OrderByDescending(beer => beer.Ratings.Average(r => r.Rating))
                        .ToListAsync();
                collection.GetDTO();
            }
            return null;
        }
    }
}
