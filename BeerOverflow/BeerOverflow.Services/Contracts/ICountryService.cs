﻿using BeerOverflow.Services.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface ICountryService
    {
        Task<IEnumerable<CountryDTO>> GetAllCountries(int itemsPerPage = 20, int pageIndex = 1);
        Task<CountryDTO> GetCountry(int id);
        Task UpdateCountry(int id, CountryDTO model);
        Task DeleteCountry(int id);
        Task CreateCountry(CountryDTO countryDTO);
        Task<int> GetCountriesCount();




    }
}
