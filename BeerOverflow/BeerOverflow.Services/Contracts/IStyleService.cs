﻿using BeerOverflow.Services.DTOEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface  IStyleService
    {
        Task CreateStyle(StyleDTO style);
        Task<StyleDTO> GetSyle(int id);
        Task EditStyle(int id, StyleDTO model);
        Task DeleteStyle(int id);
        Task<ICollection<StyleDTO>> GetAllStyles(int itemsPerPage = 5, int pageIndex = 1);
        Task<ICollection<StyleDTO>> GetAllForDisplay();
    }
}
