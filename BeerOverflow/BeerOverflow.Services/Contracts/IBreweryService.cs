﻿using BeerOverflow.Services.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IBreweryService
    {
        Task<ICollection<BeerDTO>> GetBeers(int id);
        Task<ICollection<BreweryDTO>> GetAllForDisplay();
        Task<ICollection<BreweryDTO>> GetAllBreweries(int itemsPerPage = 5, int pageIndex = 1);
        Task<BreweryDTO> GetBrewery(int id);
        Task CreateBrewery(BreweryDTO brewery);
        Task DeleteBrewery(int id);
        Task EditBrewery(int id, BreweryDTO model);
        Task<int> GetBreweriesCount();
    }
}
