﻿using BeerOverflow.Services.DTO;
using BeerOverflow.Services.DTOEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public interface IBeerServices
    {
        Task<ICollection<BeerDTO>> TopRatedBeers();
        Task<ICollection<BeerDTO>> GetAllBeers(int itemsPerPage = 5, int pageIndex = 1);
        Task<BeerDTO> GetBeer(int id);
        Task<int> GetBeersCount();
        Task<ICollection<BeerReviewDTO>> GetBeerReviews(int id);
        Task<ICollection<BeerDTO>> GetBeerByCriteria(string name, double? abv, string breweryName);
        Task<ICollection<BeerDTO>> FilterBeers(string brewery, string style);
        Task<ICollection<BeerDTO>> SortBeers(string criteria);
        Task CreateBeer(BeerDTO beer);
        Task DeleteBeer(int id);
        Task RateBeer(int id, int rating, int userId);
        Task ReviewBeer(int id, string text, int userId);
        Task EditBeer(int id, BeerDTO beer);
    
    }
}