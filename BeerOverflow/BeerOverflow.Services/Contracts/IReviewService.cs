﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface  IReviewService
    {
        Task<BeerReview> CreateReview(BeerReviewDTO review);
        BeerReviewDTO ReadReview(int id);
        BeerReviewDTO UpdateReview(int id, string name);
        BeerReviewDTO DeleteReview(int id);

    }
}
