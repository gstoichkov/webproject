﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IUserService
    {
       Task<ICollection<UserDTO>> GetAllUsers(int itemsPerPage = 5, int pageIndex = 1);
        Task<IList<User>> GetAllRegularUsers();
        Task CreateUser(UserDTO user);
        Task<UserDTO> GetUser(int id);
        Task<ICollection<BeerDTO>> GetBeerWishList(int id);
        Task AddBeerToWishlist(int userId, BeerDTO dto);
        Task UpdateUser(int id, UserDTO model);
        Task DeleteUser(int id);


    }
}
