﻿using BeerOverflow.Services.DTOEntities;
using System.Collections.Generic;

namespace BeerOverflow.Services.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int RoleId { get; set; }
        public List<BeerDTO> Wishlists { get; set; } = new List<BeerDTO>();
        public ICollection<BeerReviewDTO> Reviews { get; set; } = new List<BeerReviewDTO>();
        public ICollection<BeerRatingDTO> Ratings { get; set; } = new List<BeerRatingDTO>();

       
    }
}
