﻿namespace BeerOverflow.Services.DTOEntities
{
    public class StyleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
