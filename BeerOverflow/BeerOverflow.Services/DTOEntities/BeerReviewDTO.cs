﻿namespace BeerOverflow.Services.DTOEntities
{
    public  class BeerReviewDTO:FeedbackDTO
    {
        public string Text { get; set; }
    }
}
