﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace BeerOverflow.Services.DTO
{
    public class BeerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double ABV { get; set; }
        public BreweryDTO Brewery { get; set; }
        public int BreweryId { get; set; }

        public StyleDTO Style{ get; set; }
        public int StyleId { get; set; }
        public ICollection<BeerRatingDTO> Ratings { get; set; } = new List<BeerRatingDTO>();
        public ICollection<BeerReviewDTO> Reviews { get; set; } = new List<BeerReviewDTO>();
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}

