﻿namespace BeerOverflow.Services.DTOEntities
{
    public class BeerRatingDTO:FeedbackDTO
    {
        public int Rating { get; set; }
    }
}
