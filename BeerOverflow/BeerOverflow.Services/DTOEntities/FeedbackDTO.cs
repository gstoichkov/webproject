﻿using BeerOverflow.Services.DTO;

namespace BeerOverflow.Services.DTOEntities
{
    public abstract class FeedbackDTO
    {
        public int BeerId { get; set; }
        public int UserId { get; set; }
       
    }
}
