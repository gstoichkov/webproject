﻿using BeerOverflow.Models;
using System;
using System.Collections.Generic;

namespace BeerOverflow.Services.DTO
{
    public class BreweryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CountryDTO Country { get; set; }
        public int CountryId { get; set; }
        public ICollection<BeerDTO> Beers { get; set; } = new List<BeerDTO>();
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
        
    }
}
