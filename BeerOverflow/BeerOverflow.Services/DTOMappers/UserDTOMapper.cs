﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeerOverflow.Services.DTOMappers
{
   public static class UserDTOMapper
    {
        public static UserDTO GetDTO(this User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }
            return new UserDTO
            {
                Id = user.Id,
                Name = user.UserName,
                
            };
        }
        public static ICollection<UserDTO> GetDTO(this ICollection<User> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
