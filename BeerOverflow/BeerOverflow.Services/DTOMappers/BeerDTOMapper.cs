﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services.DTOMappers
{
    public static class BeerDTOMapper
    {
        public static BeerDTO GetDTO(this Beer item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new BeerDTO
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                ABV = item.ABV,
                StyleId = item.StyleId,
                //Brewery=item.Brewery.GetDTO(),
                BreweryId = item.BreweryId,
                CreatedOn=item.CreatedOn,
                ModifiedOn=item.ModifiedOn,
                DeletedOn=item.DeletedOn,
                IsDeleted=item.IsDeleted,
                Ratings = item.Ratings?.GetDTO(),
                Reviews = item.Reviews?.GetDTO(),
            };
        }
        public static ICollection<BeerDTO> GetDTO(this ICollection<Beer> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
