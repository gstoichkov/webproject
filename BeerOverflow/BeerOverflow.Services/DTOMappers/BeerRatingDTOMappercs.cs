﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeerOverflow.Services.DTOMappers
{
    public static class BeerRatingDTOMappers
    {
        public static BeerRatingDTO GetDTO(this BeerRating item)
        {
            if(item==null)
            {
                throw new ArgumentNullException();
            }
            return new BeerRatingDTO
            {
                Rating = item.Rating,
                UserId = item.UserId,
                BeerId=item.BeerId
            };
        }
        public static ICollection<BeerRatingDTO> GetDTO(this ICollection<BeerRating> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
