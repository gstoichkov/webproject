﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeerOverflow.Services.DTOMappers
{
    public static class BeerReviewDTOMapper 
    {
        public static BeerReviewDTO GetDTO(this BeerReview item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new BeerReviewDTO
            {
                Text = item.Text,
                UserId = item.UserId,
                BeerId = item.BeerId, 
            };
        }
        public static ICollection<BeerReviewDTO> GetDTO(this ICollection<BeerReview> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
