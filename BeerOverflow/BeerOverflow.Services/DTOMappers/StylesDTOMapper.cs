﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeerOverflow.Services.DTOMappers
{
    public static class StylesDTOMapper
    {
        public static StyleDTO GetDTO(this Style item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new StyleDTO
            {
                Id=item.Id,
                Name = item.Name,
            };
        }
        public static ICollection<StyleDTO> GetDTO(this ICollection<Style> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}