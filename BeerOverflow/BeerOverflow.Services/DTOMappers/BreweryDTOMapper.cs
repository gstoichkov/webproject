﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeerOverflow.Services.DTOMappers
{
    public static class BreweryDTOMapper
    {
        public static BreweryDTO GetDTO(this Brewery item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new BreweryDTO
            {
                Id = item.Id,
                Name = item.Name,
                Country = item.Country?.GetDTO(),
                CountryId = item.CountryId,
                Beers= item.Beers?.GetDTO(),
                CreatedOn = item.CreatedOn,
                ModifiedOn = item.ModifiedOn,
                DeletedOn = item.DeletedOn,
                IsDeleted = item.IsDeleted,
            };
        }
        public static ICollection<BreweryDTO> GetDTO(this ICollection<Brewery> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
