﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.CountryServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ReturnTrue_When_ParamsAreValid()
        {
            {
                var options = Utils.GetOptions(nameof(ReturnTrue_When_ParamsAreValid));


                var country = new Country
                {
                    Id = 1,
                };

                using (var arrangeContext = new BeerOverflowDbContext(options))
                {
                    arrangeContext.Countries.Add(country);
                    arrangeContext.SaveChangesAsync();

                }

                using (var actContext = new BeerOverflowDbContext(options))
                {
                    var sut = new CountryService(actContext);

                    var result = sut.DeleteCountry(1);

                }
                using (var assertContext = new BeerOverflowDbContext(options))
                {
                    var actual = assertContext.Countries.First(x => x.Id == 1);

                    Assert.IsTrue(actual.IsDeleted);

                }
            }
        }
    }
}
