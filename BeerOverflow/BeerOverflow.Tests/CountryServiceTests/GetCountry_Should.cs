﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.DTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Tests.CountryServiceTests
{
    [TestClass]
    public class GetCountry_Should
    {
        [TestMethod]
        public void Return_All_Countries_Count()
        {
            var options = Utils.GetOptions(nameof(Return_All_Countries_Count));


            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new CountryService(actContext);
                var countries = actContext.Countries.CountAsync();
                var result = sut.GetCountriesCount();
                Assert.AreEqual(countries, result);
            }
        }
        [TestMethod]
        public void ReturnCorrectCountryDTO_When_Id_IsCorrect()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectCountryDTO_When_Id_IsCorrect));


            var country = new Country
            {
                Id = 1,
            };


            using (var arrangeContext = new BeerOverflowDbContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();

            }

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new CountryService(actContext);
                var result = sut.GetCountry(1);
                Assert.AreEqual(country.Id, result.Result.Id);
            }
        }
        [TestMethod]
        public async Task Return_When_Country_IsCreated()
        {
            var options = Utils.GetOptions(nameof(Return_When_Country_IsCreated));

            var country = new CountryDTO
            {
                Name = "Germany"
            };

            using (var arrangeContext = new BeerOverflowDbContext(options))
            {
                var sut = new CountryService(arrangeContext);
                await sut.CreateCountry(country);
            }

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new CountryService(actContext);
                var result = sut.GetCountry(1);
                Assert.AreEqual(country.Name, result.Result.Name);

            }
        }

    }
}

