﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.Services;
using Elasticsearch.Net;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Tests.BreweryServiceTests
{
    [TestClass]
    public class GetBrewery_Should
    {
        [TestMethod]
        public void Return_Brewery_ById()
        {
            var options = Utils.GetOptions(nameof(Return_Brewery_ById));
            var providerMock = new Mock<IDateTimeProvider>();

            var brewery = new Brewery
            {
                Id = 1,
            };

            using (var arrangeContext = new BeerOverflowDbContext(options))
            {
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.SaveChanges();

            }

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new BreweryServices(actContext);
                var result = sut.GetBrewery(1);
                Assert.AreEqual(brewery.Id, result.Id);
            }
        }
        [TestMethod]
        public void Return_Breweries_Count()
        {
            var options = Utils.GetOptions(nameof(Return_Breweries_Count));
            var providerMock = new Mock<IDateTimeProvider>();

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new BreweryServices(actContext);
                var beers = actContext.Breweries.CountAsync();
                var result = sut.GetBreweriesCount();
                Assert.AreEqual(beers, result);
            }
        }
        [TestMethod]
        public void Return_Beers_From_Brewery()
        {
            var options = Utils.GetOptions(nameof(Return_Beers_From_Brewery));

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new BreweryServices(actContext);
                var beers = actContext.Breweries.First(x=>x.Id == 1);
                var result = sut.GetBeers(1);
                //WHY THEY ARE WITH DIFFERENT "ID" ?!?!?!?!?!?!? SAME WITH BEERS
                Assert.AreEqual(beers, result);
            }
        }
        [TestMethod]
        public async Task Return_When_Brewery_IsCreated()
        {
            var options = Utils.GetOptions(nameof(Return_When_Brewery_IsCreated));

            var brewery = new BreweryDTO
            {
                Id =1,
                Name = "TopBrewery"
            };

            using (var arrangeContext = new BeerOverflowDbContext(options))
            {
                var sut = new BreweryServices(arrangeContext);
                await sut.CreateBrewery(brewery);
            }

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new BreweryServices(actContext);
                var result = sut.GetBrewery(1);
                Assert.AreEqual(brewery.Id, result.Result.Id);
            }
        }
    }
}
