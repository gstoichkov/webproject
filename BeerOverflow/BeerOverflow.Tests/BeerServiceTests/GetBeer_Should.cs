﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Controllers;
using Elasticsearch.Net;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Tests.NewFolder
{
    [TestClass]
    public class GetBeer_Should
    {
        [TestMethod]
        public async Task Return_When_Beer_IsCreated()
        {
            var options = Utils.GetOptions(nameof(Return_When_Beer_IsCreated));

            var beer = new BeerDTO
            {
                Name = "Kamenitza"
            };

            using (var arrangeContext = new BeerOverflowDbContext(options))
            {
                var sut = new BeerServices(arrangeContext);
                await sut.CreateBeer(beer);
            }

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new BeerServices(actContext);
                var result = sut.GetBeer(1);
                Assert.IsTrue(beer.Name == "Kamenitza");

            }
        }
        [TestMethod]
        public void  ReturnCorrectBeerDTO_When_Id_IsCorrect()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectBeerDTO_When_Id_IsCorrect));

            var beer = new Beer
            {
                Id = 1,
            };


            using (var arrangeContext = new BeerOverflowDbContext(options))
            {
                arrangeContext.Beers.Add(beer);
            }

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new BeerServices(actContext);
                var result = sut.GetBeer(1);
                Assert.IsTrue(result.Id == 1);
            }
        }

        [TestMethod]
        public void Throw_When_BeerNotFound()
        {
            var options = Utils.GetOptions(nameof(Throw_When_BeerNotFound));

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new BeerServices(actContext);

                Assert.ThrowsException<ArgumentNullException>(() => sut.GetBeer(0));
            }
        }
        [TestMethod]
        public void Return_All_Beers()
        {
            var options = Utils.GetOptions(nameof(Throw_When_BeerNotFound));

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new BeerServices(actContext);
                var beers = actContext.Beers.ToListAsync();
                actContext.SaveChangesAsync();

                var result = sut.GetAllBeers();

                Assert.AreEqual(beers, result.Result.Count);

            }
        }
        [TestMethod]
        public void Return_All_Beers_Count()
        {
            var options = Utils.GetOptions(nameof(Return_All_Beers_Count));
            var providerMock = new Mock<IDateTimeProvider>();

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new BeerServices(actContext);
                var beers = actContext.Beers.CountAsync();
                var result = sut.GetBeersCount();
                Assert.AreEqual(beers, result);
            }
        }
        [TestMethod]
        public void Return_Beer_ByCriteria()
        {
            var options = Utils.GetOptions(nameof(Return_Beer_ByCriteria));

            var beer = new Beer
            {
                Name = "Kamenitza",
                ABV = 4.5,
 
            };

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new BeerServices(actContext);
                var beers = sut.GetBeerByCriteria("Kamenitza", 4.5, "Brewers");

                Assert.IsTrue(beer.Name == "Kamenitza");
                Assert.IsTrue(beer.ABV == 4.5);
            }
        }
    }
}
