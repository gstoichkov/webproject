﻿using BeerOverflow.Data;
using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Tests.StyleServiceTests
{
    [TestClass]
    public class CreatedStyle_Should
    {
        [TestMethod]
        public void GetCorrectStyle()
        {
            var options = Utils.GetOptions(nameof(GetCorrectStyle));

            var style = new Style
            {
                Id = 1,
            };

            using (var arrangeContext = new BeerOverflowDbContext(options))
            {
                arrangeContext.Styles.Add(style);
                arrangeContext.SaveChanges();

            }

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new StyleServices(actContext);
                var result = sut.GetSyle(1);
                Assert.AreEqual(style.Id, result.Id);
            }
        }
        [TestMethod]
        public async Task Return_When_Style_IsCreated()
        {
            var options = Utils.GetOptions(nameof(Return_When_Style_IsCreated));

            var style = new StyleDTO
            {
                Name = "NewStyle"
            };

            using (var arrangeContext = new BeerOverflowDbContext(options))
            {
                var sut = new StyleServices(arrangeContext);
                await sut.CreateStyle(style);
            }

            using (var actContext = new BeerOverflowDbContext(options))
            {
                var sut = new StyleServices(actContext);
                var result = sut.GetSyle(1);
                Assert.AreEqual(style.Name, result.Result.Name);

            }
        }
        [TestMethod]
        public async Task Return_When_Style_IsEdited()
        {
            var options = Utils.GetOptions(nameof(Return_When_Style_IsEdited));

            using (var arrangeContext = new BeerOverflowDbContext(options))
            {
                var style = new StyleDTO
                {
                    Id = 1,
                    Name = "OldStyle"
                };
                var newStyle = new StyleDTO
                {
                    Id = 1,
                    Name = "NewStyle"
                };

                var sut = new StyleServices(arrangeContext);
                style.Name = newStyle.Name;
                await sut.EditStyle(1, newStyle);

                Assert.AreEqual(style.Name, newStyle.Name);

            }
        }
    }
}

