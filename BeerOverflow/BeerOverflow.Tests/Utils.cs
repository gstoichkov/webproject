using BeerOverflow.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BeerOverflow.Tests
{
    public class Utils
    {    
        public static DbContextOptions<BeerOverflowDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<BeerOverflowDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;

        }
    }
}
