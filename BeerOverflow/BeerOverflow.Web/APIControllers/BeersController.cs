﻿using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Controllers;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeersController : ControllerBase
    {
        private readonly IBeerServices services;

        public BeersController(IBeerServices services)
        {
            this.services = services;
        }

        //GET ALL api/Beers
        //Работи и взима  по 5 бири на страница, ако не му зададеш изрично стойности!
        [HttpGet("")]
        public async Task<IActionResult> GetAllBeers(int? itemsPerPage, int? pageIndex)
        {
            if (itemsPerPage.HasValue && pageIndex.HasValue)
            {
                return Ok(await services.GetAllBeers(itemsPerPage.Value, pageIndex.Value));
            }
            else
            {
                return Ok(await services.GetAllBeers());
            }
        }

        //GET Beer api/Beers/id - работи
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBeer(int id)
        {
            return Ok(await this.services.GetBeer(id));
        }

        //CREATE Beer - работи и добавя в базата
        [HttpPost("")]
        //[Authorize(Roles = "Admin,User")]
        public  IActionResult CreateBeer([FromBody] BeerViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            var beer = new BeerDTO
            {
                Name = model.Name,
                Description = model.Description,
                ABV = model.ABV,
                BreweryId = model.BreweryId,
                StyleId = model.StyleId
            };
            services.CreateBeer(beer);
            return Created("post", model);
        }

        //EDIT api/Beers/id - работи
        [HttpPut("{id}")]
        public async Task<IActionResult> EditBeer(int id, [FromBody] BeerViewModel inputModel)
        {
            if (id < 1 || inputModel == null)
            {
                return BadRequest();
            }
            var model = new BeerDTO
            {
                Name = inputModel.Name,
                Description = inputModel.Description,
                ABV = inputModel.ABV,
                BreweryId = inputModel.BreweryId,
                StyleId = inputModel.StyleId
            };
           await services.EditBeer(id, model);
            return Ok();
        }

        //DELETE Beer by Id - работи
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBeer(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }
            await services.DeleteBeer(id);
            return Ok();
        }

        //RATE Beer -работи
        //Като е put или post заявка, трябва да се ползват модели/класове from body
        [HttpPut("rate/{id}")]
        public async Task<IActionResult> RateBeer(int id, [FromBody] BeerRatingViewModel model)
        {
            if (id < 1)
            {
                return BadRequest();
            }
          await services.RateBeer(id, model.Rating, model.UserId);
           return Ok();
        }

        //FILTER BY CRITERIA - работи
        [HttpGet("filter")]
        public async Task<IActionResult> Get([FromQuery] string brewery, string style)
        {
            return Ok(await services.FilterBeers(brewery, style));
        }

        //ORDER Beers - работи
        [HttpGet("order")]
        public async Task<IActionResult> Get([FromQuery] string criteria)
        {
            return Ok(await services.SortBeers(criteria));
        }

        //ADD BEER REVIEW - работи
        [HttpPut("review/{id}")]
        public async Task<IActionResult> ReviewBeer(int id, [FromBody] BeerReviewViewModel model)
        {
            if (id < 1)
            {
                return BadRequest();
            }
           await services.ReviewBeer(id, model.Text, model.UserId);
            return Ok();
        }
        //Get Beer Reviews -работи
        [HttpGet("review/{id}")]
        public async Task<IActionResult> GetBeerReviews(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }
            
            return Ok(await services.GetBeerReviews(id));
        }
    }
}
