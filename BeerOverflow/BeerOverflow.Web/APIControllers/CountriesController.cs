﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryService services;

        public CountriesController(ICountryService services)
        {
            this.services = services;

        }

        //GET ALL Countries -работи
        [HttpGet("")]
        public async Task<IActionResult> GetAllCountries(int? itemsPerPage, int? pageIndex)
        {
            if (itemsPerPage.HasValue && pageIndex.HasValue)
            {
                return Ok(await services.GetAllCountries(itemsPerPage.Value, pageIndex.Value));
            }
            else
            {
                return Ok(await services.GetAllCountries());
            }
        }

        //GET COUNTRY BY ID -работи
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCountryById(int id)
        {
            return Ok(await services.GetCountry(id));
        }

        //CREATE COUNTRY -работи
        [Route(nameof(CreateCountry))]
        [HttpPost]
        public async Task<IActionResult> CreateCountry([FromBody] CountryViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var country = new CountryDTO
            {
                Name = model.Name
            };
            await services.CreateCountry(country);
            return Created("post", country);
        }

        //UPDATE COUNTRY - работи
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCountry(int id, [FromBody] CountryViewModel model)
        {
            if (id < 1 || model == null)
            {
                return BadRequest();
            }
            var dto = new CountryDTO
            {
                Name = model.Name
            };
            await services.UpdateCountry(id, dto);
            return Ok();
        }

        //DELETE COUNTRY - работи
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCountry(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }
            await services.DeleteCountry(id);
            return Ok();
        }
    }
}
