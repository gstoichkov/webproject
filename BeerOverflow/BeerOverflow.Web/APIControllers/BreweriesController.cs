﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BreweriesController : ControllerBase
    {
        private readonly IBreweryService services;

        public BreweriesController(IBreweryService services)
        {
            this.services = services;
        }

        //GET ALL BREWERIES - работи
        [HttpGet("")]
        public async Task<IActionResult> GetAllBreweries(int? itemsPerPage, int? pageIndex)
        {
            if (itemsPerPage.HasValue && pageIndex.HasValue)
            {
                return Ok(await services.GetAllBreweries(itemsPerPage.Value, pageIndex.Value));
            }
            else
            {
                return Ok(await services.GetAllBreweries());
            }
        }

        //GET BREWERY BY ID - работи
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBrewery(int id)
        {
            var brewer = await services.GetBrewery(id);
            if (brewer == null)
            {
                return NotFound();
            }
            return Ok(brewer);
        }

        //CREATE BREWERY - работи
        [HttpPost("")]
        public async Task<IActionResult> CreateBrewery([FromBody] BreweryViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            var brewery = new BreweryDTO
            {
                Name = model.Name,
                CountryId = model.CountryId,
            };
           await services.CreateBrewery(brewery);
            return Created("post", model);
        }

        //UPDATE BREWERY - работи
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateBrewery(int id, [FromBody] BreweryViewModel model)
        {
            if (id < 1 || model == null)
            {
                return BadRequest();
            }
            var newBreweryDto = new BreweryDTO()
            {
                Name = model.Name,
                CountryId = model.CountryId
            };
           await services.EditBrewery(id,newBreweryDto);
            return Ok();
        }

        //DELETE BREWERY - работи
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }
            await services.DeleteBrewery(id);
            return Ok(); 
        }
    }
}

