﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOEntities;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StylesController : ControllerBase
    {
        private readonly IStyleService services;

        public StylesController(IStyleService services)
        {
            this.services = services;
        }
        //GET ALL STYLES - работи
        [HttpGet("")]
        public async Task<IActionResult> GetAllStyles(int? itemsPerPage, int? pageIndex)
        {
            if (itemsPerPage.HasValue && pageIndex.HasValue)
            {
                return Ok(await services.GetAllStyles(itemsPerPage.Value, pageIndex.Value));
            }
            else
            {
                return Ok(await services.GetAllStyles());
            }
        }

        //GET STYLE BY ID - работи
        [HttpGet("{id}")]
        public async Task<IActionResult> GetStyle(int id)
        {
            return Ok(await services.GetSyle(id));
        }

        //CREATE STYLE - работи
        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] StyleViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            var style = new StyleDTO
            {
                Name = model.Name
            };
            await services.CreateStyle(style);
            return Created("post", model);
        }

        //EDIT STYLE - работи
        [HttpPut("{id}")]
        public async Task<IActionResult> EditStyle(int id, [FromBody] StyleViewModel model)
        {
            if (id < 1 || model == null)
            {
                return BadRequest();
            }
            var newModel = new StyleDTO
            {
                Name =model.Name,
                
            };
            await services.EditStyle(id,newModel);
            return Ok();
        }

        //DELETE STYLE - работи
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }
            await services.DeleteStyle(id);
            return Ok();
        }
    }
}

