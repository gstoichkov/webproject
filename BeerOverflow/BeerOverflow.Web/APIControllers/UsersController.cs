﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    //b.Add beer to wish list
    //c.Add beer to the list of beers already drank
    //d.Get wish list beers
    //e.Get drank list beers

    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService services;

        public UsersController(IUserService services)
        {
            this.services = services;
        }

        //GET ALL Users - работи
        [HttpGet("")]
        public async Task<IActionResult> GetAllUsers(int? itemsPerPage, int? pageIndex)
        {
            if (itemsPerPage.HasValue && pageIndex.HasValue)
            {
                return Ok(await services.GetAllUsers(itemsPerPage.Value, pageIndex.Value));
            }
            else
            {
                return Ok(await services.GetAllUsers());
            }
        }

        //GET User - работи
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            return Ok(await services.GetUser(id));
        }

        //CREATE User - работи
        [HttpPost("")]   
        public async Task<IActionResult> CreateUser([FromBody] UserViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            var user = new UserDTO
            {
                Name = model.Name,
                RoleId = model.RoleId
            };
            await services.CreateUser(user);
            return Created("post", model);
        }

        //EDIT User - работи
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id, [FromBody] UserViewModel inputModel)
        {
           if(inputModel==null)
            {
                return BadRequest();
            }
            var model = new UserDTO
            {
                Name = inputModel.Name,
                RoleId = inputModel.RoleId
            };
            await services.UpdateUser(id, model);
            return Ok();
        }

        //DELETE User - работи
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }
            await services.DeleteUser(id);
            return Ok();
        }


        //ADD BEER TO WISHLIST - работи
        [HttpPut("addbeer/{id}")]
        public async Task<IActionResult> AddBeerToWishlist(int id, [FromBody]  BeerViewModel model)
        {
           if(id < 1)
            {
                return BadRequest();
            }
            var beerDTO = new BeerDTO
            {
                Id = model.Id
            };
           await services.AddBeerToWishlist(id,beerDTO);
            return Ok();
        }
        //GET BEER WISHLIST - работи
        [HttpGet("getwishlist/{id}")]
        public async Task<IActionResult> GetBeerWishlist(int id)
        {
            if (id < 1)
            {
                return BadRequest();
            }
            return Ok(await services.GetBeerWishList(id));
        }
    }
}
