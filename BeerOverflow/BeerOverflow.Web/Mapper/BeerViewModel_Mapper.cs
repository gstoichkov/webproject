﻿using BeerOverflow.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Mapper
{
    public static class BeerViewModel_Mapper
    {
        public static BeerViewModel GetDTO(this BeerViewModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException();
            }
            return new BeerViewModel
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                ABV = model.ABV,
                BreweryId = model.BreweryId,
                StyleId = model.StyleId,
                // Breweries = model.Breweries?.GetDTO(),
            };
        }

    }
}
