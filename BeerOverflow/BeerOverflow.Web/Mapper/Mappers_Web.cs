﻿using AutoMapper;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Models;

namespace BeerOverflow.Web.Mapper
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            CreateMap<CountryDTO, CountryViewModel>();

            CreateMap<CountryViewModel, CountryDTO>();

            CreateMap<BeerDTO, BeerViewModel>();

            CreateMap<BeerViewModel, BeerDTO>();

        }
    }
}