﻿using BeerOverflow.Web.Models;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Mapper
{
    public static class CountryViewModel_Mapper
    {
        public static CountryViewModel GetDTO(this CountryViewModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException();
            }
            return new CountryViewModel
            {
                Id = model.Id,
                Name = model.Name,
               // Breweries = model.Breweries?.GetDTO(),
            };
        }
    }
}
