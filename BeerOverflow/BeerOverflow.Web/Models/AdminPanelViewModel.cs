﻿using BeerOverflow.Models;
using System.Collections.Generic;

namespace BeerOverflow.Web.Controllers
{
    public class AdminPanelViewModel
    {
        public AdminPanelViewModel()
        {

        }

        public List<User> Users { get; set; } = new List<User>();
    }
}