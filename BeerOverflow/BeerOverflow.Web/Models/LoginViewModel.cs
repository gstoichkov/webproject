﻿using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Web.ViewModels
{
    public class LoginViewModel
    {
        //public LoginViewModel()
        //{

        //}

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
