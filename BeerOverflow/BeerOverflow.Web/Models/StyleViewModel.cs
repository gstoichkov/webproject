﻿namespace BeerOverflow.Web.APIControllers
{
    public class StyleViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}