﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class BeerReviewViewModel
    {
        public int UserId { get; set; }
        public string Text { get; set; }
    }
}
