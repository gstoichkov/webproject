﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class BeerRatingViewModel
    {
        public int Rating { get; set; }
        public int UserId { get; set; }
    }
}
