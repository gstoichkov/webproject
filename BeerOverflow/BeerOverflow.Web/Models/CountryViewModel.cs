﻿using BeerOverflow.Models.Abstracts;
using BeerOverflow.Services.DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Web.Models
{
    public class CountryViewModel:Entity
    {
        public int Id { get; set; }
        [Required, StringLength(10, MinimumLength = 3)]
        public string Name { get; set; }
        public ICollection<BreweryDTO> Breweries { get; set; } = new List<BreweryDTO>(); // Must be Breweries View Model

    }
}
