﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using System.Collections.Generic;

namespace BeerOverflow.Web.Models
{
    public class AllBeersViewModel
    {
        public IEnumerable<BeerDTO> Beers { get; set; } = new List<BeerDTO>();
    }
}
