﻿using BeerOverflow.Services.DTO;
using System.Collections.Generic;

namespace BeerOverflow.Web.Models
{
    public class HomeIndexViewModel
    {
        public ICollection<BeerDTO> TopRatedBeers { get; set; } = new List<BeerDTO>();
    }
}
