﻿using BeerOverflow.Services.DTO;

namespace BeerOverflow.Web.Models
{
    public class BreweryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CountryDTO Country { get; set; }
        public int CountryId { get; set; }
    }
}