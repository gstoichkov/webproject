﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using BeerOverflow.Services.DTOEntities;

namespace BeerOverflow.Web.Models
{

    public class BeerViewModel 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public StyleDTO Style { get; set; }
        public BreweryDTO Brewery { get; set; }
        public double ABV { get; set; }
        public int BreweryId { get; set; }
        public int StyleId { get; set; }

    }
}
