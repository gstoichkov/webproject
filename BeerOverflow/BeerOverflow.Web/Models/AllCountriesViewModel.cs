﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTO;
using System.Collections.Generic;

namespace BeerOverflow.Web.Models
{
    public class AllCountriesViewModel
    {
       public IEnumerable<CountryDTO> Countries { get; set; } = new List<CountryDTO>();
    }
}
