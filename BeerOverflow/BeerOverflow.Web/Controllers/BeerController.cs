﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public class BeerController : Controller
    {
        private readonly IBeerServices service;
        private readonly IBreweryService breweryServices;
        private readonly IStyleService styleServices;

        public BeerController(IBeerServices service, IBreweryService breweryServices, IStyleService styleServices)
        {
        this.breweryServices= breweryServices;
            this.styleServices=styleServices;
            this.service = service;
        }

        //GET ALL Beers
        [HttpGet]
        public async Task<IActionResult> GetAllBeers(int? itemsCount, int? pageNumber)
        {
            ViewBag.beersCount = await service.GetBeersCount();
            var beers =
                await service.GetAllBeers(itemsCount.HasValue ? itemsCount.Value : 5, pageNumber.HasValue ? pageNumber.Value : 1);
            return View(beers);

        }
        //GET Beer by ID
        public async Task<IActionResult> Details(int id)
        {     
            var beer= await service.GetBeer(id);
            var brewery = await breweryServices.GetBrewery(beer.BreweryId);
            var style = await styleServices.GetSyle(beer.StyleId);
            ViewBag.Brewery = brewery.Name;
            ViewBag.Style = style.Name;
            return View(beer);
        }

        //DELETE Beer GET
        [HttpGet]
        public async Task<IActionResult> DeleteBeer(int id)
        {
            var country = await service.GetBeer(id);
            if (country.IsDeleted == true)
            {
                return View("Error404");
            }
            return View(country);
        }

        //DELETE Beer POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await service.DeleteBeer(id);
            return RedirectToAction(nameof(GetAllBeers));
        }

        // EDIT Beer GET
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var beer = await service.GetBeer(id);
            if (beer.IsDeleted == true)
            {
                return View("Error404");
            }
            ViewData["BreweryId"] = new SelectList(await breweryServices.GetAllForDisplay(), "Id", "Name", beer.BreweryId);
            ViewData["StyleId"] = new SelectList(await styleServices.GetAllForDisplay(), "Id", "Name", beer.StyleId);
            return View(beer);

            //if (id == null)
            //{
            //    return NotFound();
            //}

            //var beer = await _context.Beers.FindAsync(id);
            //if (beer == null)
            //{
            //    return NotFound();
            //}
            //ViewData["BreweryId"] = new SelectList(_context.Breweries, "Id", "Name", beer.BreweryId);
            //ViewData["StyleId"] = new SelectList(_context.Styles, "Id", "Name", beer.StyleId);
            //return View(beer);
        }

        // POST: Beers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, BeerDTO dto)
        {
            if (ModelState.IsValid)
            {
                await service.EditBeer(id:dto.Id, dto);
                return RedirectToAction("GetAllBeers", "Beer");
            }

            var model = new BeerViewModel
            {
                Name = dto.Name,
                Description = dto.Description,
                ABV = dto.ABV,
                Style= dto.Style,
                Brewery= dto.Brewery
            };
            ViewData["BreweryId"] = new SelectList(await breweryServices.GetAllForDisplay(), "Id", "Name", model.BreweryId);
            ViewData["StyleId"] = new SelectList(await styleServices.GetAllForDisplay(), "Id", "Name", model.StyleId);
            return View(dto);
            //if (id != beer.Id)
            //{
            //    return NotFound();
            //}

            //if (ModelState.IsValid)
            //{
            //    try
            //    {
            //        _context.Update(beer);
            //        await _context.SaveChangesAsync();
            //    }
            //    catch (DbUpdateConcurrencyException)
            //    {
            //        if (!BeerExists(beer.Id))
            //        {
            //            return NotFound();
            //        }
            //        else
            //        {
            //            throw;
            //        }
            //    }
            //    return RedirectToAction(nameof(Index));
            //}
            //ViewData["BreweryId"] = new SelectList(_context.Breweries, "Id", "Name", beer.BreweryId);
            //ViewData["StyleId"] = new SelectList(_context.Styles, "Id", "Name", beer.StyleId);
            //return View(beer);
        }

        //CREATE GET Beer
        [HttpGet]
         public async Task<IActionResult> Create()
        {
            ViewData["BreweryId"] = new SelectList(await breweryServices.GetAllForDisplay(), "Id", "Name");
            ViewData["StyleId"] = new SelectList(await styleServices.GetAllForDisplay(), "Id", "Name");
            return View();
        }

        //CREATE POST Beer
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,ABV,BreweryId,StyleId,CreatedOn,ModifiedOn,DeletedOn,IsDeleted")] BeerDTO beer)
        {
            if (ModelState.IsValid)
            {
               await service.CreateBeer(beer);
                return RedirectToAction("GetAllBeers","Beer");
            }
            var model = new BeerViewModel
            {
                Name = beer.Name,
                Description = beer.Description,
                ABV = beer.ABV,
                Brewery = beer.Brewery,
                Style = beer.Style,
                StyleId= beer.StyleId,
                BreweryId=beer.BreweryId
            };
            ViewData["BreweryId"] = new SelectList(await breweryServices.GetAllForDisplay(), "Id", "Name", model.BreweryId);
            ViewData["StyleId"] = new SelectList(await styleServices.GetAllForDisplay(), "Id", "Name", model.StyleId);
            return View(model);
        }






        ////CREATE Beer - работи и добавя в базата
        //[HttpPost("")]
        ////[Authorize(Roles = "Admin,User")]
        //public IActionResult CreateBeer([FromBody] BeerViewModel model)
        //{
        //    if (model == null)
        //    {
        //        return BadRequest();
        //    }
        //    var beer = new BeerDTO
        //    {
        //        Name = model.Name,
        //        Description = model.Description,
        //        ABV = model.ABV,
        //        BreweryId = model.BreweryId,
        //        StyleId = model.StyleId
        //    };
        //    service.CreateBeer(beer);
        //    return Created("post", model);
        //}

        ////EDIT api/Beers/id - работи
        //[Route(nameof(EditBeer) + "/{id}")]
        //[HttpPost]
        //public async Task<IActionResult> EditBeer(int id,BeerViewModel inputModel)
        //{
        //    if (id < 1 || inputModel == null)
        //    {
        //        return BadRequest();
        //    }
        //    var model = new BeerDTO
        //    {
        //        Name = inputModel.Name,
        //        Description = inputModel.Description,
        //        ABV = inputModel.ABV,
        //        BreweryId = inputModel.BreweryId,
        //        StyleId = inputModel.StyleId
        //    };
        //    await service.EditBeer(id, model);
        //    return Ok();
        //}

        ////RATE Beer -работи
        ////Като е put или post заявка, трябва да се ползват модели/класове from body
        //[HttpPut("rate/{id}")]
        //public async Task<IActionResult> RateBeer(int id, [FromBody] BeerRatingViewModel model)
        //{
        //    if (id < 1)
        //    {
        //        return BadRequest();
        //    }
        //    await service.RateBeer(id, model.Rating, model.UserId);
        //    return Ok();
        //}

        ////FILTER BY CRITERIA - работи
        //[HttpGet("filter")]
        //public async Task<IActionResult> Get([FromQuery] string brewery, string style)
        //{
        //    return Ok(await service.FilterBeers(brewery, style));
        //}

        ////ORDER Beers - работи
        //[HttpGet("order")]
        //public async Task<IActionResult> Get([FromQuery] string criteria)
        //{
        //    return Ok(await service.SortBeers(criteria));
        //}

        ////ADD BEER REVIEW - работи
        //[HttpPut("review/{id}")]
        //public async Task<IActionResult> ReviewBeer(int id, [FromBody] BeerReviewViewModel model)
        //{
        //    if (id < 1)
        //    {
        //        return BadRequest();
        //    }
        //    await service.ReviewBeer(id, model.Text, model.UserId);
        //    return Ok();
        //}
        ////Get Beer Reviews -работи
        //[HttpGet("review/{id}")]
        //public async Task<IActionResult> GetBeerReviews(int id)
        //{
        //    if (id < 1)
        //    {
        //        return BadRequest();
        //    }

        //    return Ok(await service.GetBeerReviews(id));
        //}
    }
}
