﻿using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IBeerServices beerServices;

        public HomeController(ILogger<HomeController> logger, IBeerServices beerServices)
        {
            _logger = logger;
           this.beerServices = beerServices;
        }

        public async Task<IActionResult> Index()  
        {
            var collection = await beerServices.TopRatedBeers();
            var model = new HomeIndexViewModel
            {
                TopRatedBeers = collection
            };

            return View(model);
        }
        public IActionResult About()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
