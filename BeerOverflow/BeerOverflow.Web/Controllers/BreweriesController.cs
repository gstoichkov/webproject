﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public class BreweriesController : Controller
    {
        private readonly IBreweryService service;
        private readonly ICountryService countryServices;
        private readonly IBeerServices beerServices;

        public BreweriesController(IBreweryService service,ICountryService countryServices, IBeerServices beerServices)
        {
            this.service = service;
            this.countryServices = countryServices;
            this.beerServices = beerServices;
        }

        // GET: All Breweries
        [HttpGet]
        public async Task<IActionResult> Index(int? itemsCount, int? pageNumber)
        {
            ViewBag.breweriescount = await service.GetBreweriesCount();
            var countries =
                await service.GetAllBreweries(itemsCount.HasValue ? itemsCount.Value : 5, pageNumber.HasValue ? pageNumber.Value : 1);
            return View(countries);
        }

        // GET: Brewery by Id
        public async Task<IActionResult> Details(int id)
        {
            var brewery = await service.GetBrewery(id);
           // var collection = await service.GetBeers(id);
           // ViewData["Beers"] = collection; //new SelectList(await service.GetBeers(id), "Id", "Name", brewery.Beers);
            return View(brewery);
        }

        // Update Brewery GET
        //[Route(nameof(Update) + "/{id}")]
        //[HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            var brewery = await service.GetBrewery(id);
            if (brewery.IsDeleted == true)
            {
                return View("Error404");
            }

            ViewData["CountryId"] = new SelectList(await countryServices.GetAllCountries(), "Id", "Name", brewery.CountryId);

            return View(brewery);
        }

        // Update Brewery POST
        //[Route(nameof(Update) + "/{id}")]
        [HttpPost]
        public async Task<IActionResult> Update([Bind("Id,Name,CountryId")] BreweryDTO breweryDTO)
        {
            if (ModelState.IsValid)
            {
                await service.EditBrewery(id:breweryDTO.Id,breweryDTO);
                return RedirectToAction("Index", "Breweries");
            }

            var dto = new BreweryViewModel
            {
                Name = breweryDTO.Name,
                CountryId = breweryDTO.CountryId
            };
            ViewData["CountryId"] = new SelectList(await countryServices.GetAllCountries(), "Id", "Name", dto.CountryId);
            return View(dto);
        }

        // DELETE Brewery GET
        [HttpGet]
        public async Task<IActionResult> DeleteBrewery(int id)
        {
            var brewery = await service.GetBrewery(id);
            if (brewery.IsDeleted == true)
            {
                return View("Error404");
            }
            return View(brewery);
        }

        // DELETE Brewery POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await service.DeleteBrewery(id);
            return RedirectToAction(nameof(Index));
        }

        //CREATE Brewery GET
       //[Route(nameof(Create))]
       [HttpGet]
        public async Task<IActionResult> AddBrewery()
        {
            ViewData["CountryId"] = new SelectList(await countryServices.GetAllCountries(), "Id", "Name");//dto.CountryId);
            return View();
        }

       //CREATE Brewery POST
       //[Route(nameof(Create))]
       [HttpPost]
        public async Task<IActionResult> AddBrewery([Bind("Id,Name,CountryId")] BreweryDTO model)
        {
            if (ModelState.IsValid)
            {
                await service.CreateBrewery(model);
                return RedirectToAction("Index", "Breweries");
            }

            var dto = new BreweryViewModel
            {
                Name = model.Name,
                CountryId = model.CountryId,
                //Country = model.Country
            };
            ViewData["CountryId"] = new SelectList(await countryServices.GetAllCountries(), "Id", "Name", dto.CountryId);
            return RedirectToAction("Index", "Breweries");
            // return RedirectToAction("Index", "Brewery");
        }

    }
}






