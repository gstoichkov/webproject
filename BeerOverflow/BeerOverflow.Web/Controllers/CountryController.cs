﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTO;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.Web.CodeGenerators.Mvc.View;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public class CountryController : Controller
    {
        private readonly ICountryService service;

        public CountryController(ICountryService service)
        {
            this.service = service;
        }

        //GET ALL Countries
        [HttpGet]
        public async Task<IActionResult> GetAllCountries(int? itemsCount,int? pageNumber)
        {
            ViewBag.countriesCount = await service.GetCountriesCount();
            var countries = 
                await service.GetAllCountries(itemsCount.HasValue?itemsCount.Value:5,pageNumber.HasValue?pageNumber.Value:1);
            return View(countries);
        }

        //GET Country
        public async Task<IActionResult> Details(int id)
        {
                return View(await service.GetCountry(id)); 
        }

        //Update Country GET
        [Route(nameof(UpdateCountry) + "/{id}")]
        [HttpGet]
        public async Task<IActionResult> UpdateCountry(int id)
        {
            var country = await service.GetCountry(id);
            if (country.IsDeleted == true)
            {
                return View("Error404");
            }
            return View(country);

        }

        // Update Country POST
        [Route(nameof(UpdateCountry) + "/{id}")]
        [HttpPost]
        public async Task<IActionResult> UpdateCountry(int id, CountryViewModel model)
        {
            if (id < 0 || model == null)
            {
                return BadRequest();
            }
            var dto = new CountryDTO
            {
                Name = model.Name
            };
            await service.UpdateCountry(id, dto);
            return RedirectToAction("GetAllCountries", "Country");
        }

        // Create Country GET
        [Route(nameof(CreateCountry))]
        [HttpGet]
        public IActionResult CreateCountry()
        {
            return View();
        }

        // Create Country POST
        [Route(nameof(CreateCountry))]
        [HttpPost]
        public async Task<IActionResult> CreateCountry(CountryDTO countryViewModel)
        {
            await service.CreateCountry(countryViewModel);
            return RedirectToAction("GetAllCountries", "Country");
        }

        // Delete Country GET
        [HttpGet]
        public async Task<IActionResult> DeleteCountry(int id)
        {
            var country = await service.GetCountry(id);
            if(country.IsDeleted==true)
            {
                return View("Error404");
            }
            return View(country);
        }

        // Delete Country POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //var country = await service.GetCountry(id);
            await service.DeleteCountry(id);
            return RedirectToAction(nameof(GetAllCountries));
        }
       
    }
}
